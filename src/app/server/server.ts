const express = require('express');
const bodyParser  = require('body-parser');
const cors = require('cors');
const app = express();
const config = require('../config.json');
const UUID = require('uuidjs');

app.use(cors());
app.use(bodyParser.json());

app.listen(4201, () => {
  console.log('Server started on port 4201...');
});

app.route('/api').get((req, res) => {
  res.send(data);
});

// TODO: change to use name after post for location is available
app.route('/api/locations/:location').get((req, res) => {
  if (req.params.location === '1') {
    res.send(locations[0]);
  } else if (req.params.location === '2') {
    res.send(locations[1]);
  }
});

app.route('/api/getById/:id').get((req, res) => {
  res.send(findDeep(req.params.id));
});

app.route('/api/togglePower/:id?').patch((req, res) => {
  const entity = togglePower(req.params.id);
  entity === '404' ? res.status(404).send('BAD REQUEST: Item not found') : res.send(entity);
});

app.route('/api/toggleIce').post(async (req, res) => {
  await res.send(toggleIce());
});

app.route('/api/find/:id').get((req, res) => {
  const item = findDeep(req.params.id);
  res.send(item);
});

const applications = [];

function getRandom(type, min, max) {
  switch (true) {
    case type === 'integer': return Math.floor(Math.random() * (max - min + 1) + min);
    case type === 'float': return (Math.random() * (max - min) + min);
    case type === 'bool': return Math.random() >= 0.5;
    default: return null;
  }
}

function calculateSum(objects, prop) {
  return objects.reduce((prev, curr) => prev + curr[prop], 0);
}

function camelCase(str) {
  return str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
}

function findDeep(itemId?: string) {
    switch (true) {
      case data.locations.some(l => l.id === itemId):
        return data.locations.find(l => l.id === itemId);
      case data.locations.some(l => l.rows.some(r => r.id === itemId)):
        return data.locations
          .find(l => l.rows.find(r => r.id === itemId)).rows
          .find(row => row.id === itemId);
      case data.locations.some(l => l.rows.some(r => r.racks.some(k => k.id === itemId))):
        return data.locations
          .find(l => l.rows.find(r => r.racks.find(k => k.id === itemId))).rows
          .find(rr => rr.racks.find(kk => kk.id === itemId)).racks
          .find(rack => rack.id === itemId);
      case data.locations.some(l => l.rows.some(r => r.racks.some(k => k.apps.some(a => a.uuid === itemId)))):
        return data.locations
          .find(l => l.rows.find(r => r.racks.find(k => k.apps.find(a => a.uuid === itemId)))).rows
          .find(r => r.racks.find(k => k.apps.find(a => a.uuid === itemId))).racks
          .find(rack => rack.apps.find(a => a.uuid === itemId)).apps
          .find(aa => aa.uuid === itemId);
      case typeof(itemId) === 'string':
        return '404';
      default:
        return data;
    }
}

const handleBattery = (entityId) => {
  const row = findDeep(entityId);
  let count;
  let counter = row.batterySOC;

  const discharge = () => {
    counter = counter > 0 && counter > row.usage ? counter - row.usage : 0;
    row.batterySOC = counter;
    if (counter === 0) {
      row.racks.map(r => r.status = 'off');
      stop();
    }
  };

  const charge = () => {
    counter = counter < 100 ? (counter + 1) : 100;
    row.batterySOC = counter;
    if (counter === 100) { stop(); }
  };

  count = row.powerEnabled === false ? setInterval(discharge, (1000)) : setInterval(charge, (1000));
  const stop = () => clearInterval(count);
};

const togglePower = (id) => {
  const entity = findDeep(id);
  if (id !== 'undefined') {
    switch (true) {
      case entity === '404':
        return '404';
      case entity.racks && (entity.powerEnabled === true || entity.powerEnabled === false):
        entity.powerEnabled = !entity.powerEnabled;
        handleBattery(entity.id);
        const status = entity.powerEnabled === true
          ? 'on'
          : entity.powerEnabled === false && entity.batterySOC > 0 ? 'batt' : 'off';
        entity.racks.map(r => r.status = status);
        return entity;
      default:
        entity.powerEnabled = !entity.powerEnabled;
        if (!entity.powerEnabled) { entity.rows.map(r => togglePower(r.id)); }
        return entity;
    }
  } else {
    data.powerEnabled = !data.powerEnabled;
    if (data.powerEnabled === false) { data.locations.map(l => togglePower(l.id)); }
    return data;
  }
};

const moveApps = (apps) => {
  apps.map(a => {
    const rack = findDeep(a.topology.rack.id);
    const movedApp = rack.apps.find(ax => ax.id === a.id);
    movedApp.status = 'outgoing';
  });

  const move = () => {
    // console.log('move called, apps.length: ', apps.length);
    setTimeout(() => {
      const sourceRack = findDeep(apps[0].topology.rack.id);
      sourceRack.usage -= apps[0].usage;
      sourceRack.apps.splice(sourceRack.apps.indexOf(apps[0], 1));

      const destination = findDeep(apps[0].updatedLocation.rack);
      console.log('destination: ', destination.apps);
      const movedApp = destination.apps.find(da => da.id === apps[0].id);
      movedApp.moved = true;
      movedApp.status = 'running';
      destination.usage += apps[0].usage;
      apps.shift();
      if (apps.length > 0) {
        move();
      }
    }, apps[0].usage * 5 * 1000);
  };

  move();
};

const consolodateApps = (id: string) => {
  const loc = data.locations.find(l => l.id === id);
  const availableApps = data.applications.filter(a => a.topology.location.id === loc.id);
  const movedApps = [];
  loc.rows[0].racks.map(r => {
    availableApps.splice(r.apps[r.apps[0].id - 1], r.apps.length);
    let usage = 0 + r.usage;
    let currentApp = availableApps[availableApps.length - 1];
    while (availableApps.length > 0 && usage + currentApp.usage < r.limit && r.apps.length < 12) {
      if ((currentApp.usage + r.usage) < r.limit) {
        const updatedApp = {
          ...currentApp,
          status: 'incoming',
          updatedLocation: {
            location: currentApp.location,
            row: currentApp.row,
            rack: r.id
          }
        };
        console.log('updatedApp::::: ', updatedApp);
        r.apps.push(updatedApp);
        movedApps.push(updatedApp);
        usage += currentApp.usage;
        availableApps.splice(availableApps.indexOf(currentApp, 1));
        currentApp = availableApps[availableApps.length - 1];
      }
    }
    // r.usage = usage;
  });
  moveApps(movedApps);
};

const toggleIce = () => {
  data.iceEnabled = !data.iceEnabled;
  if (data.iceEnabled === true) {
    data.locations.map(l => {
      switch (true) {
        case l.rows.length === 1 :
          consolodateApps(l.id);
          break;
      }
    });
  }
  return data;
};

const makeApps = (id, limit, rackName, rackId, rackTopology) => {
  let usage = 0;
  const apps = [];

  while (usage <= (limit * .4) && apps.length < 10) {
    const currentUsage = getRandom('float', 0.15, 1);
    const currentApp = {
      id: applications.length + 1,
      uuid: UUID.generate(),
      usage: currentUsage,
      status: 'running',
      moved: false,
      topology: {
        ...rackTopology,
        rack: { id: rackId, name: rackName }
      },
      updatedLocation: null
    };
    apps.push(currentApp);
    applications.push(currentApp);
    usage += Number(currentUsage);
  }
  return apps;
};

const makeRacks = (qty, rowName, rowId, rowTopology) => {
  const racks = [];

  while (racks.length < qty) {
    const id = UUID.generate();
    const name = `Rack ${racks.length + 1}`;
    const topology = {
      ...rowTopology,
      row: { id: rowId, name: rowName}
    };
    const rackLimit = config.locations.find(l => l.name === rowTopology.location.name).rackLimit;
    const apps = makeApps(1, rackLimit, name, id, topology);
    const usage = calculateSum(apps, 'usage');
    const utilization = usage / rackLimit;
    racks.push({
      name,
      id,
      limit: rackLimit,
      status: 'on',
      apps,
      usage,
      utilization,
      topology,
      powerEnabled: true
    });
  }
  return racks;
};

const makeRows = (qty, racksQty, locationId, locationName) => {
  const rows = [];

  while (rows.length < qty) {
    const id = UUID.generate();
    const topology = {
      location: {id: locationId, name: locationName}
    };
    const name = `Row ${rows.length + 1}`;
    const racks = makeRacks(racksQty, name, id, topology);
    const usage = calculateSum(racks, 'usage');
    const limit = calculateSum(racks, 'limit');
    const utilization = usage / limit;
    rows.push({
      name,
      id,
      racks,
      usage,
      limit,
      utilization,
      batterySOC: 100,
      powerEnabled: true,
      topology
    });
  }
  return rows;
};

const makeLocation = (name, rowQty, rackQty) => {
  const id = UUID.generate();
  const rows = makeRows(rowQty, rackQty, id, name);
  const usage = calculateSum(rows, 'usage');
  const limit = calculateSum(rows, 'limit');
  const utilization = usage / limit;
  const location = {
    name,
    id,
    rows,
    usage,
    limit,
    utilization,
    powerEnabled: true
  };
  return location;
};

const locations = config.locations.map(l => makeLocation(l.name, l.rows, l.racksPerRow));

const data = {
  iceEnabled: config.iceEnabled,
  powerEnabled: config.powerEnabled,
  locations,
  applications
};

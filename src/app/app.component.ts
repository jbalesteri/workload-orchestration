import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { LocationService } from '@app/services/location.service';
import { interval } from 'rxjs';
import { Logger } from '@app/core';
const log = new Logger('appComponent');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'workload-orchestration';
  navOpened: boolean;
  datacenter: any;
  powerEnabled: boolean;
  iceEnabled: boolean;
  locations: Array<object>;


  constructor(
    private locationService: LocationService
  ) { }

  togglePower() {
    this.locationService.togglePower().subscribe(res => {
      this.datacenter = res;
      this.powerEnabled = res.powerEnabled;
    });
  }

  toggleIce() {
    this.locationService.toggleIce().subscribe(res => {
      this.datacenter = res;
      this.iceEnabled = res.iceEnabled;
    });
  }

  ngOnInit() {
    let initialized = false;
    interval(10000).subscribe(() => {
      this.locationService
        .getAllData()
        .subscribe(res => {
          if (initialized === false) { log.debug('datacenter: ', res); }
          initialized = true;
          this.datacenter = res;
          this.locations = res.locations.map(l => ({ id: l.id, name: l.name }));
          this.powerEnabled = res.powerEnabled;
          this.iceEnabled = res.iceEnabled;
        });
    });
  }
}

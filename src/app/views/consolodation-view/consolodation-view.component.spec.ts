import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolodationViewComponent } from './consolodation-view.component';

describe('ConsolodationViewComponent', () => {
  let component: ConsolodationViewComponent;
  let fixture: ComponentFixture<ConsolodationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolodationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolodationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

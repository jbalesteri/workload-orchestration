import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Logger } from '@app/core';
import { map, catchError } from 'rxjs/operators';
const log = new Logger('services: location');

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  constructor(private http: HttpClient) { }

  get(rowQty: number): Observable<any> {
    return this.http
      .get<string>(`http://localhost:4201/api/locations/${rowQty}`)
      .pipe(catchError(error => error));
  }

  getAllData(): Observable<any> {
    return this.http
      .get<string>(`http://localhost:4201/api/`)
      .pipe(catchError(error => error));
  }

  getById(id: string): Observable<any> {
    return this.http
      .get<string>(`http://localhost4201/api/getById/${id}`)
      .pipe(catchError(error => error));
  }

  togglePower(id?: string) {
    return this.http
      .patch<string>(`http://localhost:4201/api/togglePower/${id}`, {})
      .pipe(
        map((resp: any) => resp),
        catchError((error: any) => error)
      );
  }

  toggleIce() {
    return this.http
      .post<string>(`http://localhost:4201/api/toggleIce/`, {})
      .pipe(
        map((resp: any) => resp),
        catchError((error: any) => error)
      );
  }
}


export function getRandom(type: 'integer' | 'float' | 'bool', min?: number, max?: number) {
    switch (true) {
        case type === 'integer':
            return Math.floor(Math.random() * (max - min + 1) + min);
        case type === 'float':
            return (Math.random() * (max - min) + min);
        case type === 'bool':
            return Math.random() >= 0.5;
        default:
            return null;

    }
}

export function calculateSum(objects, prop) {
    return objects.reduce((prev, curr) => prev + curr[prop], 0);
}

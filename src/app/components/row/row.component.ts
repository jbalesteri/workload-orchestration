import { Component, OnInit, Input } from '@angular/core';
import { interval } from 'rxjs';
import { LocationService } from '@app/services/location.service';
import { Logger } from '@app/core';
const log = new Logger('row');

@Component({
  selector: 'app-row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss']
})
export class RowComponent implements OnInit {
  @Input() row: any;

  constructor( private locationService: LocationService ) { }

  togglePower() {
    this.locationService.togglePower(this.row.id).subscribe(res => {
      this.row = res;
    });
  }

  ngOnInit() {}

}

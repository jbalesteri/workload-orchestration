import { Component, OnInit, Input } from '@angular/core';
import { interval } from 'rxjs';
import { Logger } from '@app/core';
import { LocationService } from '@app/services/location.service';
const log = new Logger('locationContainer');

@Component({
  selector: 'app-location-container',
  templateUrl: './location-container.component.html',
  styleUrls: ['./location-container.component.scss']
})
export class LocationContainerComponent implements OnInit {
  @Input() rowQty: number;
  location: any;
  powerEnabled: boolean;

  constructor( private locationService: LocationService ) { }

  togglePower() {
    this.locationService.togglePower(this.location.id).subscribe(res => {
      this.location = res;
    });
  }

  ngOnInit() {
    let initialized = false;
    interval(1000).subscribe(() => {
      this.locationService
      .get(this.rowQty)
      .subscribe(res => {
        if (initialized === false) { log.info('location: ', res); }
        initialized = true;
        this.location = res;
      });
    });
  }
}

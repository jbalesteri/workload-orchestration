import { Component, OnInit, Input } from '@angular/core';
import { LocationService } from '@app/services/location.service';
import { Logger } from '@app/core';
import { summaryFileName } from '@angular/compiler/src/aot/util';
const log = new Logger('rack');

@Component({
  selector: 'app-rack',
  templateUrl: './rack.component.html',
  styleUrls: ['./rack.component.scss']
})
export class RackComponent implements OnInit {
  @Input() rack: any;

  constructor(private locationService: LocationService) { }

  toggleRackPower() {
    this.locationService.togglePower(this.rack.id).subscribe(res => {
      log.debug('updated rack: ', res);
    });
  }

  ngOnInit() {}

}

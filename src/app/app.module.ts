import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LocationService } from './services/location.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RackComponent } from './components/rack/rack.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCardModule, MatChipsModule, MatIconModule, MatToolbarModule,
  MatSidenavModule, MatListModule, MatProgressSpinnerModule, MatTooltipModule, MatSlideToggleModule,
  MatButtonToggleModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { RowComponent } from './components/row/row.component';
import { LocationContainerComponent } from './components/location-container/location-container.component';
import { ConsolodationViewComponent } from './views/consolodation-view/consolodation-view.component';
import { MigrationViewComponent } from './views/migration-view/migration-view.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    RackComponent,
    RowComponent,
    LocationContainerComponent,
    ConsolodationViewComponent,
    MigrationViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [LocationService],
  bootstrap: [AppComponent]
})
export class AppModule { }

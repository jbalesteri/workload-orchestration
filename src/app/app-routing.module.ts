import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsolodationViewComponent } from './views/consolodation-view/consolodation-view.component';
import { MigrationViewComponent } from './views/migration-view/migration-view.component';

const routes: Routes = [
  {
    path: '',
    component: ConsolodationViewComponent
  },
  {
      path: 'migration',
      component: MigrationViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
